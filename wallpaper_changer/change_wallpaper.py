#!/usr/bin/python3

import json
import urllib.request
import os
import random

directory = os.path.expanduser("~/Pictures/wallpapers")
reddit_url = "https://www.reddit.com/r/wallpaper+wallpapers.json"

def get_json():
	page = urllib.request.urlopen(reddit_url)
	raw_json = page.read().decode('utf-8')
	return json.loads(raw_json)

def get_reddit_posts_from_json(json):
	posts = []
	for post in json.get('data').get('children'):
		posts.append(post.get('data'))
	return posts

def download_image_from_post(post):
	images = post.get('preview').get('images')
	image_url = images[0].get('source').get('url')
	image_name = image_url.rsplit('/', 1)[-1]
	file_name = directory + "/" + image_name
	with urllib.request.urlopen(image_url) as response, open(file_name, 'wb') as out_file:
		data = response.read()
		out_file.write(data)
	return os.path.abspath(file_name)

def post_has_image(post):
	return 'preview' in post

try:
	if not os.path.exists(directory):
		os.makedirs(directory)
	json = get_json()
	posts = get_reddit_posts_from_json(json)

	post = random.choice(posts)
	try_counter = 0
	while not post_has_image(post) and try_counter <= 10:
		post = random.choice(posts)
		try_counter += 1
	
	image_path = download_image_from_post(post)
	os.system("gsettings set org.gnome.desktop.background picture-uri file://" + image_path)
	print("New wallpaper has been set")
except:
	print("Too many reddit requests, please wait a minute")

